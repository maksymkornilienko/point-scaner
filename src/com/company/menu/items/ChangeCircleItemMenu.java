package com.company.menu.items;

import com.company.Circle;
import com.company.Point;
import com.company.menu.MenuItem;
import com.company.services.circle.CircleService;

import java.util.Scanner;

public class ChangeCircleItemMenu extends MenuItem {
    Scanner scanner;
    CircleService circleService;
    Circle circle;

    public ChangeCircleItemMenu(Scanner scanner, CircleService circleService) {
        this.scanner = scanner;
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Change Circle";
    }

    @Override
    public void execute() {
        System.out.println("Add x");
        double x=scanner.nextDouble();
        System.out.println("Add y");
        double y=scanner.nextDouble();
        System.out.println("Add radius");
        double radius=scanner.nextDouble();
        scanner.nextLine();

        circle= new Circle(new Point(x,y),radius);
        circleService.addCircle(circle);
    }
}
