package com.company.menu.items;

import com.company.Point;
import com.company.menu.MenuItem;
import com.company.services.circle.CircleService;
import com.company.services.point.PointService;

public class ShowInsideCircleItemMenu extends MenuItem {
    PointService pointService;
    CircleService circleService;

    public ShowInsideCircleItemMenu(PointService pointService, CircleService circleService) {
        this.pointService = pointService;
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Show inside circle";
    }

    @Override
    public void execute() {
        if (circleService.getCircle().getCenter()==null){
            System.out.println("Set Circle params");
            return;
        }
        for (int i = 0; i < pointService.getPoints().length; i++) {
            Point point=pointService.getPoints()[i];
            boolean contain=circleService.getCircle().containsPoint(point);
            if (contain){
                System.out.println(point.toString());
            }
        }
    }
}
