package com.company.menu.items;

import com.company.menu.MenuItem;

public class ExitItemMenu extends MenuItem {
    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void execute() {
        System.out.println("Good bye!!!");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
