package com.company.menu.items;

import com.company.Point;
import com.company.menu.MenuItem;
import com.company.services.circle.CircleService;
import com.company.services.point.PointService;

public class ShowOutsideCircleItemMenu extends MenuItem {
    PointService pointService;
    CircleService circleService;

    public ShowOutsideCircleItemMenu(PointService pointService, CircleService circleService) {
        this.pointService = pointService;
        this.circleService = circleService;
    }

    @Override
    public String getName() {
        return "Show outside circle";
    }

    @Override
    public void execute() {
        for (int i = 0; i < pointService.getPoints().length; i++) {
            Point point=pointService.getPoints()[i];
            boolean contain=circleService.getCircle().containsPoint(point);
            if (!contain){
                System.out.println(point.toString());
            }
        }
    }
}
