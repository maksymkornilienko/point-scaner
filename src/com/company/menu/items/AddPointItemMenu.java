package com.company.menu.items;

import com.company.Point;
import com.company.menu.MenuItem;
import com.company.services.point.PointService;

import java.util.Scanner;

public class AddPointItemMenu extends MenuItem {
    Scanner scanner;
    PointService pointListService;

    public AddPointItemMenu(Scanner scanner, PointService pointListService) {
        this.scanner = scanner;
        this.pointListService = pointListService;
    }

    @Override
    public String getName() {
        return "Add point";
    }

    @Override
    public void execute() {
        System.out.println("Add x");
        double x=scanner.nextDouble();
        System.out.println("Add y");
        double y=scanner.nextDouble();
        pointListService.addPoint(new Point(x,y));
    }
}
