package com.company.menu;

import java.util.Scanner;

public class Menu {
    Scanner scanner;
    MenuItem[] items;

    public Menu( Scanner scanner, MenuItem[] items) {
        this.scanner = scanner;
        this.items = items;
    }

    public void run(){
        for (;;){
            showMenu();
            int choice=getUserChoice();
            if (choice < 0 || choice>= items.length){
                System.out.println("Incorrect choice");
                continue;
            }
            items[choice].execute();
            if (items[choice].isFinal()) break;
        }
    }

    private int getUserChoice() {
        System.out.println("Enter your choice");
        int ch=scanner.nextInt();
        scanner.nextLine();
        return ch-1;
    }

    private void showMenu() {
        System.out.println("---------------------------------------");
        for (int i = 0; i < items.length; i++) {
            items[i].getName();
            System.out.printf("%2d - %s\n",i+1, items[i].getName());
        }
        System.out.println("---------------------------------------");
    }
}
