package com.company.menu;

public abstract class MenuItem {
    public abstract String getName();
    public abstract void execute();
    public boolean  isFinal(){
        return false;
    }
}
