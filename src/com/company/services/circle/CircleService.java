package com.company.services.circle;

import com.company.Circle;
import com.company.Point;

public interface CircleService {
    void addCircle(Circle circle);
    Circle getCircle();
}
