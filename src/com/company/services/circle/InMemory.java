package com.company.services.circle;

import com.company.Circle;
import com.company.Point;
import com.company.services.point.PointService;

import java.util.Arrays;

public class InMemory implements CircleService {
    private Circle circle = new Circle();


    @Override
    public void addCircle(Circle circle) {
        this.circle=circle;
    }

    @Override
    public Circle getCircle() {
        return circle;
    }
}
