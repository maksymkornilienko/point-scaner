package com.company.services.point;

import com.company.Point;

public interface PointService {
    void addPoint(Point point);
    Point[] getPoints();
}
