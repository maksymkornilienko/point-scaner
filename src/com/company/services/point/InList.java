package com.company.services.point;

import com.company.Point;

import java.util.Arrays;

public class InList implements PointService{
    private Point[] pointsList = new Point[]{};
    @Override
    public void addPoint(Point point) {
        int index=pointsList.length;
        int newLength=pointsList.length+1;
        Point[] copyPoint = Arrays.copyOf(pointsList, newLength);
        copyPoint[index]=point;
        pointsList = copyPoint;
    }

    @Override
    public Point[] getPoints() {
        return pointsList;
    }
}
