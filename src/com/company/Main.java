package com.company;

import com.company.menu.Menu;
import com.company.menu.MenuItem;
import com.company.menu.items.*;
import com.company.services.circle.CircleService;
import com.company.services.circle.InMemory;
import com.company.services.point.InList;
import com.company.services.point.PointService;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PointService pointService = new InList();
        CircleService circleService = new InMemory();
        Menu menu = new Menu(scanner, new MenuItem[]{
             new AddPointItemMenu(scanner, pointService),
             new ChangeCircleItemMenu(scanner,circleService),
             new ShowInsideCircleItemMenu(pointService, circleService),
             new ShowOutsideCircleItemMenu(pointService, circleService),
             new ExitItemMenu(),
        });
        menu.run();

    }
}
